import { createElement } from '../helpers/domHelper';
import { renderArena } from './arena';
import versusImg from '../../../resources/versus.png';
import { createFighterPreview } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFighterDetails } from '../../commonTypes/interfaces';
import { Positions } from '../../commonTypes/enums';

export type SelectedFightersType = [IFighterDetails, IFighterDetails];
export type SelectFighterType = (fighterId: string) => Promise<void>

export function createFightersSelector(): SelectFighterType {
  let playerOne: IFighterDetails, playerTwo: IFighterDetails;

  return async (fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);

    if(!playerOne) {
      playerOne = fighter;
    } else if(!playerTwo) {
      playerTwo = fighter;
    }
    
    renderSelectedFighters([playerOne, playerTwo]);
  };
}

const fighterDetailsMap = new Map<string, IFighterDetails>();

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId) as IFighterDetails;
  }

  try {
    const fighterDetails = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap.set(fighterId, fighterDetails);
    return fighterDetails;
  } catch (error) {
    throw error;
  }
}

function renderSelectedFighters(selectedFighters: Partial<SelectedFightersType>): void {
  const fightersPreview = document.querySelector('.preview-container___root') as HTMLElement;
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(Positions.Left, playerOne);
  const secondPreview = createFighterPreview(Positions.Right, playerTwo);
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview.innerHTML = '';
  fightersPreview.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: Partial<SelectedFightersType>): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => renderArena(selectedFighters as SelectedFightersType);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });

  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });

  const disabledClassName = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledClassName}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}
