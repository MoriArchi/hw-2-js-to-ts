import { Positions, AttackTypes } from '../../commonTypes/enums';

export function updateHealthIndicator(currentHealth: number, health: number, position: Positions): void {
  const healthIndicator = document.getElementById(`${position}-fighter-indicator`) as HTMLElement;

  const indicatorWidth = Math.max(0, (currentHealth * 100) / health);
  healthIndicator.style.width = `${indicatorWidth}%`;
}

export function toggleShield(show: boolean, position: Positions): void {
  const shield = document.getElementById(`${position}-shield`) as HTMLElement;

  shield.style.visibility = show ? 'visible' : 'hidden';
}

export function showAttack(position: Positions, attack: AttackTypes): void {
  const attackElement = document.getElementById(`${position}-${attack}`) as HTMLElement;
  attackElement.classList.add(`arena___${position}-${attack}-show`);
  setTimeout(() => {
    attackElement.classList.remove(`arena___${position}-${attack}-show`);
  }, 300);
}

export function toggleCritSignal(show: boolean, position: Positions): void {
  const indicator = document.getElementById(`${position}-crit-signal`) as HTMLElement;

  indicator.style.visibility = show ? 'visible' : 'hidden';
}

export function updateRageIndicator(position: Positions, width: number): void {
  const rageIndicator = document.getElementById(`${position}-rage-indicator`) as HTMLElement;
  rageIndicator.style.width = `${width}%`;
}

export const createCritPointsUpdatedHandler = (position: Positions) => (currentPoints: number, canCrit: boolean) => {
  if (currentPoints === 0) {
    toggleCritSignal(false, position);
  }

  if (canCrit) {
    toggleCritSignal(true, position);
  }

  updateRageIndicator(position, currentPoints * 10);
};

export const createIsBlockingChangedHandler = (position: Positions) => (isBlocking: boolean) => {
  toggleShield(isBlocking, position)
};

export const createIsDamageReceivedHandler = (position: Positions) => (currentHealth: number, health: number) => {
  updateHealthIndicator(currentHealth, health, position);
};

export const createIsAttackingHandler = (position: Positions) => (attack: AttackTypes) => {
  showAttack(position, attack)
};

export interface ICreateFighterConfigs {
  onPointsUpdated: (currentPoints: number, canCrit: boolean) => void,
  onIsBlockingChanged: (isBlocking: boolean) => void,
  onDamageReceived: (currentHealth: number, health: number) => void,
  onAttacking: (attack: AttackTypes) => void
};

export const createFighterConfigs = (position: Positions): ICreateFighterConfigs => ({
  onPointsUpdated: createCritPointsUpdatedHandler(position),
  onIsBlockingChanged: createIsBlockingChangedHandler(position),
  onDamageReceived: createIsDamageReceivedHandler(position),
  onAttacking: createIsAttackingHandler(position)
});
