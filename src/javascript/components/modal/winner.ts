import { showModal } from './modal';
import { IFighterDetails } from '../../../commonTypes/interfaces';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter: IFighterDetails): void {
  const imageElement = createFighterImage(fighter);
  const modalElement = {
    title: `${fighter.name.toUpperCase()} won!!!`,
    bodyElement: imageElement,
    onClose: () => {
      location.reload();
    }
  };

  showModal(modalElement);
}

