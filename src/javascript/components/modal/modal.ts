import { createElement } from '../../helpers/domHelper';

interface IModalArgs {
 title: string
 bodyElement: HTMLElement
 onClose(): void
}

export function showModal({ title, bodyElement, onClose = () => {} }: IModalArgs): void {
  const root = document.getElementById('root');
  const modal = createModal({ title, bodyElement, onClose }); 
  
  if(root) {
    root.append(modal);
  }
}

function createModal({ title, bodyElement, onClose }: IModalArgs): HTMLElement {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title, onClose);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

function createHeader(title: IModalArgs["title"], onClose: IModalArgs["onClose"]): HTMLElement {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span', className: 'modal-title' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }

  closeButton.addEventListener('click', close);
  headerElement.append(titleElement, closeButton);
  
  return headerElement;
}

function hideModal(): void {
  const modal = document.getElementsByClassName('modal-layer')[0];
  
  if(modal) {
    modal.remove();
  }
}
