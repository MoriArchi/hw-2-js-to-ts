import { controls, GAME_CONTROL_KEYS } from '../../constants/controls';
import { CRIT_POINTS_NEEDED_TO_CRIT_HIT } from '../../constants/fightConstants';
import { Positions, AttackTypes } from '../../commonTypes/enums';
import { getRandomFloatFromRange } from '../helpers/getRandomFloatFromRange';
import { createFighterConfigs, ICreateFighterConfigs } from './fightViewChanges';
import { IFighterDetails } from '../../commonTypes/interfaces';

interface IPlayerFighterDetails extends IFighterDetails {
  currentHealth: number,
  currentCritPoints: number,
  isBlocking: boolean,
  timerId: number,
  receiveDamage: {(value: number): void },
  setIsBlocking: {(value: boolean): void },
  doAttack: {(defender: IPlayerFighterDetails, damage: number): void },
  doCritAttack: {(defender: IPlayerFighterDetails): void },
  isCanDoCrit: {(): boolean },
  restartCritPoints: {(): void}
}

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): Promise<IFighterDetails>  {
  
  return new Promise((resolve) => {
    const firstArenaFighter: IPlayerFighterDetails = createArenaFighter(
      firstFighter,
      createFighterConfigs(Positions.Left),
    );
    const secondArenaFighter: IPlayerFighterDetails = createArenaFighter(
      secondFighter,
      createFighterConfigs(Positions.Right),
    );

    firstArenaFighter.restartCritPoints();
    secondArenaFighter.restartCritPoints();

    const pressedKeys = new Map<string, boolean>();

    const keyDownListener = (e: KeyboardEvent) => {
      if (e.repeat || !GAME_CONTROL_KEYS.some((key: string) => key === e.code)) return;

      pressedKeys.set(e.code, true);

      processFightAction(firstArenaFighter, secondArenaFighter, pressedKeys, e.code);

      if (firstArenaFighter.currentHealth <= 0) {
        removeKeyListeners();
        resolve(secondFighter);
      } else if (secondArenaFighter.currentHealth <= 0) {
        removeKeyListeners();
        resolve(firstFighter);
      }
    };

    const keyUpListener = (e: KeyboardEvent) => {
      if (e.code === controls.PlayerOneBlock) {
        firstArenaFighter.setIsBlocking(false);
      }
      if (e.code === controls.PlayerTwoBlock) {
        secondArenaFighter.setIsBlocking(false);
      }
      pressedKeys.delete(e.code);
    };

    document.addEventListener('keydown', keyDownListener);
    document.addEventListener('keyup', keyUpListener);

    const removeKeyListeners = () => {
      document.removeEventListener('keydown', keyDownListener);
      document.removeEventListener('keyup', keyUpListener);
    };
  });
}

function createArenaFighter(fighter: IFighterDetails, configs: ICreateFighterConfigs): IPlayerFighterDetails {
  const { onPointsUpdated, onIsBlockingChanged, onDamageReceived, onAttacking } = configs;

  return {
    ...fighter,
    currentHealth: fighter.health,
    currentCritPoints: 0,
    isBlocking: false,
    timerId: 0,
    receiveDamage(value: number) {
      this.currentHealth -= value;
      onDamageReceived(this.currentHealth, this.health);
    },
    setIsBlocking(value: boolean) {
      this.isBlocking = value;
      onIsBlockingChanged(value);
    },
    doAttack(defender: IPlayerFighterDetails, damage: number) {
      defender.receiveDamage(damage);
      onAttacking(AttackTypes.Punch);
    },
    doCritAttack(defender: IPlayerFighterDetails) {
      if (!this.isCanDoCrit()) return;

      this.restartCritPoints();
      defender.receiveDamage(this.attack * 2);
      onAttacking(AttackTypes.Fireball);
    },
    isCanDoCrit() {
      return this.currentCritPoints === CRIT_POINTS_NEEDED_TO_CRIT_HIT;
    },
    restartCritPoints() {
      this.currentCritPoints = 0;
      onPointsUpdated(this.currentCritPoints, false);

      this.timerId = window.setInterval(() => {
        this.currentCritPoints++;

        const canDoCrit = this.isCanDoCrit();

        onPointsUpdated(this.currentCritPoints, canDoCrit);

        if (canDoCrit) {
          clearInterval(this.timerId);
        }
      }, 1000);
    },
  };
}

function processFightAction(
  firstFighter: IPlayerFighterDetails,
  secondFighter: IPlayerFighterDetails,
  keyMap: Map<string, boolean>,
  currentCode: string
): void {
  if (currentCode === controls.PlayerOneBlock) {
    firstFighter.setIsBlocking(true);
  }
  if (currentCode === controls.PlayerTwoBlock) {
    secondFighter.setIsBlocking(true);
  }
  if (currentCode === controls.PlayerOneAttack) {
    applyFighterAttack(firstFighter, secondFighter, keyMap);
    return;
  }
  if (currentCode === controls.PlayerTwoAttack) {
    applyFighterAttack(secondFighter, firstFighter, keyMap);
    return;
  }
  if (controls.PlayerOneCriticalHitCombination.every(code => keyMap.has(code))) {
    firstFighter.doCritAttack(secondFighter);
    return;
  }
  if (controls.PlayerTwoCriticalHitCombination.every(code => keyMap.has(code))) {
    secondFighter.doCritAttack(firstFighter);
  }
}

function applyFighterAttack(attacker: IPlayerFighterDetails, defender: IPlayerFighterDetails, keyMap?: Map<string, boolean>): void {
  if (attacker.isBlocking) {
    return;
  }

  if (defender.isBlocking) {
    attacker.doAttack(defender, 0);
    return;
  }

  attacker.doAttack(defender, getDamage(attacker, defender));
}

function getDamage(attacker: IFighterDetails, defender: IFighterDetails): number {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

function getHitPower(fighter: IFighterDetails): number{
  const criticalHitChance = getRandomFloatFromRange(1, 2);
  return fighter.attack * criticalHitChance;
}

function getBlockPower(fighter: IFighterDetails): number {
  const dodgeChance = getRandomFloatFromRange(1, 2);
  return fighter.defense * dodgeChance;
}
