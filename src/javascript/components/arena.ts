import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { SelectedFightersType } from './fighterSelector';
import { IFighterDetails } from '../../commonTypes/interfaces';
import { Positions, AttackTypes } from '../../commonTypes/enums';
import { ALL_POSITIONS } from '../../constants/common';

export async function renderArena(selectedFighters: SelectedFightersType): Promise<void> {
  const root = document.getElementById('root') as HTMLElement;
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  try {
    const winner = await fight(...selectedFighters);
    showWinnerModal(winner);
  } catch (error) {
    throw error;
  }
}

function createArena(selectedFighters: SelectedFightersType): HTMLElement {
  const arena = createElement({tagName: 'div', className: 'arena___root'});
  const healthIndicators = createHealthIndicators(selectedFighters);
  const fighters = createFighters(selectedFighters);
  const shields = createShields();
  const punches = createPunches();
  const fireballs = createFireballs();
  const critSignals = createCritSignals();
  const rageIndicators = createRageIndicators();

  arena.append(healthIndicators, rageIndicators, fighters, shields, punches, fireballs, critSignals);
  return arena;
}

function createHealthIndicators(selectedFighters: SelectedFightersType): HTMLElement {
  const [firstFighter, secondFighter] = selectedFighters;
  const healthIndicators = createElement({tagName: 'div', className: 'arena___fight-status'});
  const versusSign = createElement({tagName: 'div', className: 'arena___versus-sign'});
  const leftFighterIndicator = createHealthIndicator(firstFighter, Positions.Left);
  const rightFighterIndicator = createHealthIndicator(secondFighter, Positions.Right);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: Positions): HTMLElement {
  const { name } = fighter;
  const container = createElement({tagName: 'div', className: 'arena___fighter-indicator'});
  const fighterName = createElement({tagName: 'span', className: 'arena___fighter-name'});
  const indicator = createElement({tagName: 'div', className: 'arena___health-indicator'});
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: {id: `${position}-fighter-indicator`}
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(selectedFighters: SelectedFightersType): HTMLElement {
  const [firstFighter, secondFighter] = selectedFighters;
  const battleField = createElement({tagName: 'div', className: `arena___battlefield`});
  const firstFighterElement = createFighter(firstFighter, Positions.Left);
  const secondFighterElement = createFighter(secondFighter, Positions.Right);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: Positions): HTMLElement {
  const imgElement = createFighterImage(fighter);

  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter arena___${position}-fighter`
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

function createShields(): HTMLElement {
  const container = createElement({tagName: 'div', className: `arena___shields-container`});

  const shields: HTMLElement[] = ALL_POSITIONS.map(createShield);

  container.append(...shields);
  return container;
}

function createShield(position: Positions): HTMLElement {
  const imgElement = createShieldImage();

  const shieldElement = createElement({
    tagName: 'div',
    className: `arena___${position}-shield`,
    attributes: {id: `${position}-shield`}
  });

  shieldElement.append(imgElement);
  return shieldElement;
}

function createShieldImage(): HTMLElement {
  const attributes = {
    src: '../../resources/shield.png',
    alt: 'shield'
  };

  return createElement({
    tagName: 'img',
    className: 'shield-img',
    attributes,
  });
}

function createPunches(): HTMLElement {
  const container = createElement({tagName: 'div', className: `arena___punches-container`});

  const punches: HTMLElement[] = ALL_POSITIONS.map(createPunch);

  container.append(...punches);
  return container;
}

function createPunch( position: Positions): HTMLElement {
  const imgElement = createPunchImage();
  
  const fistElement = createElement({
    tagName: 'div',
    className: `arena___${position}-punch`,
    attributes: {id: `${position}-punch`}
  });

  fistElement.append(imgElement);
  return fistElement;
}

function createPunchImage(): HTMLElement {
  const attributes = {
    src: '../../resources/punch.png',
    alt: AttackTypes.Punch
  };

  return createElement({
    tagName: 'img',
    className: 'punch-img',
    attributes,
  });
}

function createFireballs(): HTMLElement {
  const container = createElement({tagName: 'div', className: `arena___fireballs-container`});

  const fireballs: HTMLElement[] = ALL_POSITIONS.map(createFireball);

  container.append(...fireballs);
  return container;
}

function createFireball(position: Positions): HTMLElement {
  const imgElement = createFireballImage();
  
  const fireballElement = createElement({
    tagName: 'div',
    className: `arena___${position}-fireball`,
    attributes: {id: `${position}-fireball`}
  });

  fireballElement.append(imgElement);
  return fireballElement;
}

function createFireballImage(): HTMLElement {
  const attributes = {
    src: '../../resources/fireball.gif',
    alt: AttackTypes.Fireball
  };

  return createElement({
    tagName: 'img',
    className: 'fireball-img',
    attributes,
  });
}

function createCritSignals(): HTMLElement {
  const container = createElement({tagName: 'div', className: `arena___crit-signals-container`});

  const critSignals: HTMLElement[] = ALL_POSITIONS.map(createCritSignal); 

  container.append(...critSignals);
  return container;
}

function createCritSignal(position: Positions): HTMLElement {
  const imgElement = createFireballImage();
  
  const critSignalElement = createElement({
    tagName: 'div',
    className: `arena___${position}-crit-signal`,
    attributes: {id: `${position}-crit-signal`}
  });

  critSignalElement.append(imgElement);
  return critSignalElement;
}

function createRageIndicators(): HTMLElement {
  const rageIndicatorsContainer = createElement({tagName: 'div', className: 'arena___rage-indicators'});

  const rageIndicators: HTMLElement[] = ALL_POSITIONS.map(createRageIndicator);

  rageIndicatorsContainer.append(...rageIndicators);
  return rageIndicatorsContainer;
}

function createRageIndicator(position: Positions): HTMLElement {
  const container = createElement({tagName: 'div', className: 'arena___rage-indicator'});
  const rageStatus = createElement({tagName: 'span', className: 'arena___rage-status'});
  const indicator = createElement({tagName: 'div', className: 'arena___fighter-rage'});
  const bar = createElement({
    tagName: 'div',
    className: 'arena___rage-bar',
    attributes: {id: `${position}-rage-indicator`}
  });

  bar.innerText = 'Rage';
  indicator.append(bar);
  container.append(rageStatus, indicator);

  return container;
}