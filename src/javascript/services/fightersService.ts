import { callApi } from '../helpers/apiHelper';
import { IFighter, IFighterDetails } from '../../commonTypes/interfaces';

class FighterService {
  async getFighters(): Promise<IFighter[]>  {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult as IFighter[];
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id: string): Promise<IFighterDetails> {
    try {
      const endpoint = `details/fighter/${id}.json`;
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult as IFighterDetails;
    } catch (error) {
      throw error;
    }
  }
}

export const fighterService = new FighterService();
