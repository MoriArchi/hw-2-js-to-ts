export enum AttackTypes {
    Punch =  'punch',
    Fireball = 'fireball'
};

export enum Positions  {
    Left = 'left',
    Right =  'right'
};
