import { Positions } from "../commonTypes/enums";

export const ALL_POSITIONS = Object.values(Positions);
